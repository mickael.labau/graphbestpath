## A sample project for handling djkstra algorithm

## Compiling / How to
* Run bootstrap.bat
* Open file GraphBestPath.sln with Visual studio 2019
* Run project...

## Introduction

This program support multiple parameters (with these default arguments if ommited)
* --file=data/graph3.json : the json file containing graph data
* --export=1 : enable the export to graphviz on console
* --from=0 : tell the starting node in json file
* --from=4 : tell the target node in json file
* --metric=km : tell the cost metric to use (there is also "co2")

## Project's knowledge

Building has been tested only in windows. 
The project has been created using normal C++ project in Visual studio 2019 With these additional changes: 
* Add the libs/rapidjson/include folder in default list for include
* Change default version of langage to C++14 (C++17 is not compatible with rapidjson)