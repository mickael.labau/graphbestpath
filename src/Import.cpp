#include <fstream>
#include <iostream>

#include "Import.h"

#include "rapidjson/document.h"
#include "rapidjson/reader.h"
#include "rapidjson/prettywriter.h"
#include "rapidjson/filereadstream.h"
#include "rapidjson/filewritestream.h"
#include "rapidjson/error/en.h"

using namespace rapidjson;

static Document* LoadInputGraph(const char* const filename);
static const char* ToString(const Value& v);

Graph Import::FromJsonFile(const char* const filename)
{
    // No choice : we hae ot use a pointer here (see LoadInputGraph' comment)
    Document* json = LoadInputGraph(filename);
    auto errmsg = std::string("File \"") + filename + "\" does not contains an array for the json top element as expected!";

    if (!json->IsArray())
        throw std::runtime_error(errmsg);
    try
    {
        const rapidjson::Value& froms = *json;
        // C++ 17 guarantee there will be no copy of graph on return
        // (which could be big) (https://en.cppreference.com/w/cpp/language/copy_elision)
        Graph g(froms.Size());

        for (unsigned int i = 0; i < froms.Size(); i++)
        {
            const rapidjson::Value& tos = froms[i];
            if (!tos.IsArray())
                throw std::runtime_error(errmsg);
            for (unsigned int k = 0; k < tos.Size(); k++)
            {
                auto distance_km = tos[k]["distance_km"].GetDouble();
                auto co2_g = tos[k]["co2_g"].GetDouble();
                auto vertex_id = tos[k]["vertex_id"].GetInt();
                g.SetEdgeValues(i, vertex_id, distance_km, co2_g);
            }
        }
        delete json;
        return g;
    }
    catch (...)
    {
        delete json;
        throw;
    }
}

// Returning a pointer is mandatory here, Document class does not allow copy constructor
static Document* LoadInputGraph(const char* const filename)
{
    // using std::ifstream instead FILE* cause template compilation issue with ParseStream in rapidjson
    // so... let's do it old school
#pragma warning(suppress : 4996) // Because fopen_s (which is strongly recommended by compiler and raise compilation error) gives "permission denied" issue at runtime
    FILE* fp = fopen(filename, "rb, ccs=UTF-8");
    if (!fp)
    {
        char errmsg[1000];
        strerror_s(errmsg, sizeof(errmsg), errno);
        throw std::runtime_error(std::string("File \"") + filename + "\":\n" + errmsg);
    }
    assert(fp);
    char buffer[1000];
    FileReadStream is(fp, buffer, sizeof(buffer));
    auto doc = new Document();
    try
    {
        doc->ParseStream(is);
        fclose(fp);
        return doc;
    }
    catch (...)
    {
        fclose(fp);
        delete doc;
        throw;
    }
}

// only for debug
static const char* ToString(const Value& v)
{
    rapidjson::StringBuffer buffer;
    rapidjson::Writer<rapidjson::StringBuffer> writer(buffer);
    v.Accept(writer);
    return buffer.GetString();
}
