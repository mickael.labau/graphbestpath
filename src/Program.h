#pragma once
// GraphBestPath.cpp : Ce fichier contient la fonction 'main'. L'ex�cution du programme commence et se termine � cet endroit.
//

#include <functional>
#include <iostream>

#include "CommandLineSwitches.h"
#include "Import.h"
#include "Graph.h"
#include "PathAlgorithms.h"

using namespace std;

class Program
{
public:

    void Run(CommandLineSwitches& switches)
    {
        Graph graph = Import::FromJsonFile(switches.Filename);

        if (switches.Export)
            cout << graph.ToGraphvizString();

        if (switches.fromNode != switches.toNode &&
            switches.fromNode != -1 && switches.toNode != -1 &&
            switches.fromNode >= 0 && switches.fromNode < graph.GetNodeCount() &&
            switches.toNode >= 0 && switches.toNode < graph.GetNodeCount())
        {
            auto bestPath = FindBestPath(graph, switches);
            DisplayBestPath(switches.fromNode, bestPath);
        }
        else
            cout << "Invalid start and end node!" << endl;
    }

    list<pair<EdgeData, int>>* FindBestPath(Graph& graph, CommandLineSwitches& switches)
    {
        // Prepare the binding to graph storage for algorithm
        PathAlgorithm<int, EdgeData>::Traits traits;
        const std::vector<int> nodes = graph.GetAllNodes(); // fix the node list
        traits.GetNodes = [&nodes]() { return nodes; };
        traits.GetOutgoingEdges = [&graph](const int& node) { return (const std::vector<std::pair<const EdgeData&, int>>&)graph.GetOutgoingEdges(node); };
        traits.TryGetEdge = [&graph](const int& from, const int& to) { auto edge = graph.GetEdgeData(from, to); return edge->IsEmpty() ? nullptr : edge; };
        switch (switches.Metric)
        {
        case eMetric::km: traits.GetCost = [](const EdgeData& edge) -> double { return edge.distance_km; }; break;
        case eMetric::co2: traits.GetCost = [](const EdgeData& edge) -> double { return edge.co2_g; }; break;
        default:  throw std::runtime_error(string("eMetric value not handled!"));
        }

        auto algorithm = new Dijkstra<int, EdgeData>(traits);
        auto path = algorithm->FindBestPath(switches.fromNode, switches.toNode);
        return path;
    }

    void DisplayBestPath(int startNode, list<pair<EdgeData, int>>* path)
    {
        if (path == nullptr)
            cout << "No path found! Node are isolated!";
        else
        {
            std::cout << endl;
            std::cout << "  // Full Path : node " + Tools::ToString(startNode);
            for (auto const& pair : *path)
            {
                std::cout << " -> node ";
                std::cout << Tools::ToString(pair.second);
                std::cout << " ";
                std::cout << pair.first.ToString();
            }
        }
    }

};
