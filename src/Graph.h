#pragma once

#include <vector>
#include <string>

#include "Tools.h"
#include "Matrix2D.h"
#include "EdgeData.h"

using namespace std;

class Graph
{
private:
    Matrix2D<EdgeData> _matrix;

    void CheckBounds(int nodeFrom, int nodeTo) const
    {
        if (nodeFrom < 0 || nodeFrom >= _matrix.Size())
            throw std::runtime_error("Argument nodeFrom out of range!");
        if (nodeTo < 0 || nodeTo >= _matrix.Size())
            throw std::runtime_error("Argument nodeTo out of range!");
    }

public:

    Graph(int vertexCount)
        : _matrix(vertexCount)
    {
    }

    int GetNodeCount() const
    {
        return _matrix.Size();
    }

    std::vector<int> GetAllNodes() const
    {
        std::vector<int> nodes;
        nodes.reserve(10);
        for (int i = 0; i < _matrix.Size(); ++i)
            nodes.push_back(i);
        return nodes;
    }

    /// <summary>
    /// Get list of node accessible from "nodeFrom"
    /// Pairs contain edgedata and the node where the edgeData points to
    /// </summary>
    /// <remarks>
    /// Would it be interesting to use the equivalent of a state machine here instead of vector (for memory performance) ? 
    /// https://stackoverflow.com/questions/7213839/equivalent-in-c-of-yield-in-c
    /// </remarks>
    const std::vector<std::pair<const EdgeData&, int>> GetOutgoingEdges(const int nodeFrom) const
    {
        std::vector<std::pair<const EdgeData&, int>> res;
        res.reserve(5); // 5 = just a guessed value. Average valence of graph in estimated graph in input

        for (int i = 0; i < _matrix.Size(); ++i)
        {
            const EdgeData& edge = _matrix[nodeFrom][i];
            if (!edge.IsEmpty())
                res.push_back(std::pair<const EdgeData&, int>(edge, i));
        }
        return res;
    }


    bool IsDirectedGraph() const
    {
        for (int i = 0; i < _matrix.Size(); ++i)
            for (int k = 0; k < _matrix.Size(); ++k)
                if (!_matrix[i][k].Equals(_matrix[k][i]))
                    return false;
        return true;
    }

    double GetEdgeDist(int nodeFrom, int nodeTo) const 
    {
        CheckBounds(nodeFrom, nodeTo);
        return _matrix[nodeFrom][nodeTo].distance_km;
    }

    double GetEdgeCo2(int nodeFrom, int nodeTo) const
    {
        CheckBounds(nodeFrom, nodeTo);
        return _matrix[nodeFrom][nodeTo].distance_km;
    }
    const EdgeData* GetEdgeData(int nodeFrom, int nodeTo)
    {
        CheckBounds(nodeFrom, nodeTo);
        EdgeData* edge = &_matrix[nodeFrom][nodeTo];
        return edge;
    }

    void SetEdgeValues(int nodeFrom, int nodeTo, double distance_km, double co2_g)
    {
        CheckBounds(nodeFrom, nodeTo);
        if (distance_km <= 0)
            throw std::runtime_error("Argument distance_km ouf of range!");
        // If user use a bicycle, or just walk between node, value 0 would be the appropriate answer here
        // (we do not count his breath O2 => C02 of course)
        // I just dont want to bother with this value in algorithm yet (cause it could creates edge cases).
        // so for this use case we probably will use a tiny value very near 0.
        if (co2_g <= 0)
            throw std::runtime_error("Argument co2_g ouf of range!");
        auto edge = &_matrix[nodeFrom][nodeTo];
        edge->distance_km = distance_km;
        edge->co2_g = co2_g;
    }

    std::string ToGraphvizString()
    {
        std::string res("// Just copy and paste all this text in edotor.net and see the resulting graph\n");
        bool isDirectedGraph = IsDirectedGraph();
        if (isDirectedGraph)
            res += "di";
        res += "graph g\n";
        res += "{\n";
        res += "  layout = circo;\n";
        res += "  label = \"\\nNote: dist are in km and co2 in g\";\n";
        res += "  graph[nodesep = 1, ranksep = 1]; // default values are 0.25 and 0.5\n";
        res += "\n";

        for (int i = 0; i < _matrix.Size(); ++i)
            res += "  " + std::string("n") + Tools::ToString(i) + " [label=\"" + Tools::ToString(i) + "\"]; \n";
        res += "\n";

        for (int i = 0; i < _matrix.Size(); ++i)
            for (int k = isDirectedGraph ? 0 : i + 1; k < _matrix.Size(); ++k)
            {
                auto edge = _matrix[i][k];
                if (edge.IsEmpty())
                    continue;
                res += "  ";
                res += std::string("n") + Tools::ToString(i);
                res += isDirectedGraph ? " -> " : " -- ";
                res += "n" + Tools::ToString(k);
                res += " [";
                res += "label=\" dist: ";
                res += Tools::ToString(edge.distance_km);
                res += "\\n co2: ";
                res += Tools::ToString(edge.co2_g);
                res += "\"";
                res += "]\n";
            }
        res += "}";
        return res;
    }
};

