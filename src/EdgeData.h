#pragma once

#include <string>

using namespace std;


struct EdgeData
{
    double distance_km = 0;
    double co2_g = 0;

    bool Equals(const EdgeData& e) const
    {
        return distance_km == e.distance_km
            && co2_g == e.co2_g;
    }

    bool IsEmpty() const { return Equals(NoData()); }
    static const EdgeData& NoData()
    {
        static const EdgeData _NoData;
        return _NoData;
    }

    string ToString() const
    {
        return string("(")
            + "distance_km: " + Tools::ToString(distance_km)
            + ", co2_g: " + Tools::ToString(co2_g)
            + ")";
    }
};