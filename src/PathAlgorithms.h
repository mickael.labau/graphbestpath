#pragma once

#include <cassert>
#include <vector>
#include <list>
//#include <unordered_map>

#include "Matrix2D.h"

using namespace std;



template<typename TNode, typename TEdge>
class PathAlgorithm
{
public:

	// Make the algorithm independant of the graph's storage management
	// This is also to play with function< > which are new for me (I stopped programming C++ in 2006)
	struct Traits
	{
		function<const vector<TNode>()> GetNodes;
		function<const vector<pair<const EdgeData&, int> >(const TNode&)> GetOutgoingEdges;
		function<const EdgeData* (const TNode& from, const TNode& to)> TryGetEdge;
		function<double(const TEdge&)> GetCost;
	};

protected:
	const Traits        _traits;

public:

	// note : instead of vector type we should use the equivalent of IEnumerable in C# (which is probably an iterator provider)
	// That way child classes could browse big graphs without having to handle a lot memory.
	PathAlgorithm(Traits traits)
		: _traits(traits)
	{
	}

	/// <summary>
	/// Return the best path of edge from node "from" to node "to".
	/// If no path exist null is returned
	/// </summary>
	virtual list<pair<TEdge, TNode>>* FindBestPath(TNode from, TNode to) const = 0;
};



template<typename TNode, typename TEdge>
class Dijkstra : public PathAlgorithm<TNode, TEdge>
{
public:
	Dijkstra(PathAlgorithm<TNode, TEdge>::Traits traits)
		: PathAlgorithm<TNode, TEdge>(traits)
	{
	}

	list<pair<TEdge, TNode>>* FindBestPath(TNode from, TNode to) const override
	{
		auto& traits = PathAlgorithm<TNode, TEdge>::_traits;
		const vector<TNode> nodes = traits.GetNodes();
		auto n = nodes.size();

		// Transform node ref into index
		int src = FindNodeIndex(nodes, from);
		int target = FindNodeIndex(nodes, to);

		double* dist  = new double[n]; // The output array.  dist[i] will hold the shortest distance from src to i 
		bool* sptSet = new bool[n]; // sptSet[i] will be true if vertex i is included in shortest 
									// path tree or shortest distance from src to i is finalized 
		int* prev = new int[n];

		// Initialize all distances as INFINITE and stpSet[] as false 
		for (unsigned int i = 0; i < n; i++)
		{
			dist[i] = DBL_MAX;
			sptSet[i] = false;
			prev[i] = -1;
		}
		// Distance of source vertex from itself is always 0 
		dist[src] = 0;

		// Find shortest path for all vertices 
		for (unsigned int count = 0; count < n - 1; count++)
		{
			// Pick the minimum distance vertex from the set of vertices not 
			// yet processed. u is always equal to src in the first iteration. 
			int u = PickNearestNodeFromOriginNotInSet(dist, sptSet, n);

			// Mark the picked vertex as processed 
			sptSet[u] = true;

			// Update dist value of the adjacent vertices of the picked vertex. 
			for (unsigned int v = 0; v < n; v++)
			{
				// Update dist[v] only if is not in sptSet, there is an edge from 
				// u to v, and total weight of path from src to  v through u is 
				// smaller than current value of dist[v] 
				const EdgeData* edge = traits.TryGetEdge(nodes[u], nodes[v]);
				if (edge == nullptr)
					continue;
				if (v == u) // Defensive condition to ignore cycle (edge should have been null)
					continue;

				double cost = traits.GetCost(*edge);
				if (!sptSet[v] && dist[v] > dist[u] + cost)
				{
					dist[v] = dist[u] + cost;
					prev[v] = u;
				}
			}
		}
		auto res = new list<pair<TEdge, TNode>>();
		while (target != from)
		{
			auto edge = traits.TryGetEdge(prev[target], target);
			res->push_front(pair<TEdge, TNode>(*edge, nodes[target]));
			target = prev[target];
		}

		return res;
	}

	int FindNodeIndex(const vector<TNode> &nodes, TNode node) const
	{
		for (unsigned int i = 0; i < nodes.size(); ++i)
			if (nodes[i] == node)
				return i;
		throw std::runtime_error("Argument metric out of range!");
	}
	// A utility function to find the vertex with minimum distance value, from 
	// the set of vertices not yet included in shortest path tree 
	int PickNearestNodeFromOriginNotInSet(double* dist, bool* sptSet, unsigned int nodeCount) const
	{
		// Initialize min value 
		double min = DBL_MAX;
		unsigned int min_index;

		for (unsigned int v = 0; v < nodeCount; v++)
			if (sptSet[v] == false && dist[v] <= min) // <= is required and not < for first step
			{
				min = dist[v];
				min_index = v;
			}

		return min_index;
	}
};

