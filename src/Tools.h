#pragma once

#include <string>
#include <sstream>

class Tools
{
public:
	static std::string ToString(int i)
	{
		std::ostringstream strs;
		strs << i;
		return strs.str();
	}

	static std::string ToString(double dbl)
	{
		std::ostringstream strs;
		strs << dbl;
		return strs.str();
	}

	static bool StartsWith(const char* sentence, const char* prefix)
	{
		return strncmp(sentence, prefix, strlen(prefix)) == 0;
	}
};