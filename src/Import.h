#pragma once

#include "Graph.h"

class Import
{
public:
    static Graph  FromJsonFile(const char* const filename);
};
