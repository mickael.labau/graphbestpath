#pragma once

#include "Tools.h"

enum class eMetric
{
    km = 0,
    co2 = 1
};

struct CommandLineSwitches
{
    const char* Filename = 0;
    bool        Export = true;
    eMetric     Metric = eMetric::km;
    int         fromNode = -1;
    int         toNode = -1;

    CommandLineSwitches(int argc, char* argv[])
    {
        for (int i = 1; i < argc; ++i)
            if (Tools::StartsWith(argv[i], "--file="))
                Filename = argv[i] + 7;
            else if (Tools::StartsWith(argv[i], "--export="))
                Export = std::stoi(argv[i] + 6) != 0;
            else if (Tools::StartsWith(argv[i], "--from="))
                fromNode = std::stoi(argv[i] + 7);
            else if (Tools::StartsWith(argv[i], "--to="))
                toNode = std::stoi(argv[i] + 5);
            else if (Tools::StartsWith(argv[i], "--metric="))
                if (strcmp(argv[i] + 9, "km") == 0)
                    Metric = eMetric::km;
                else if (strcmp(argv[i] + 9, "co2") == 0)
                    Metric = eMetric::co2;
                else
                    throw std::runtime_error("Argument metric out of range!");
            else
                throw std::runtime_error(std::string("Unknown command line argument \"") + argv[i] + "\"");
    }
};
