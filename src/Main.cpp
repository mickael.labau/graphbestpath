// GraphBestPath.cpp : Ce fichier contient la fonction 'main'. L'exécution du programme commence et se termine à cet endroit.
//

#include <functional>
#include <iostream>

#include "Program.h"

using namespace std;


int main(int argc, char* argv[])
{
    try
    {
        CommandLineSwitches switches(argc, argv);
        if (!switches.Filename)
        {
            switches.Filename = "data\\graph3.json";
            if (switches.fromNode == -1)
                switches.fromNode = 0;
            if (switches.toNode == -1)
                switches.toNode = 4;
        }

        Program p;
        p.Run(switches);
        return 0;
    }
    catch (const exception& ex)
    {
        cout << "Unexpected exception: " << ex.what() << endl;
        return 1;
    }
    catch (...)
    {
        cout << "Unexpected exception of unknown type!" << endl;
        return 2;
    }
}
