#pragma once

#include <stdexcept>

template<typename Element>
class Matrix2D
{
    int _n;
    Element** _matrix;

    void CleanMemory()
    {
        for (int i = 0; i < _n; ++i)
            if (_matrix[i] != nullptr)
                delete _matrix[i];
        delete _matrix;
    }

public:

    Matrix2D(int n)
    {
        _n = n;
        _matrix = new Element*[_n];
        try
        {
            memset(_matrix, 0, _n * sizeof(Element*));
            for (int i = 0; i < _n; ++i)
            {
                _matrix[i] = new Element[_n];
                memset(_matrix[i], 0, _n * sizeof(Element));
            }
        }
        catch (...) // for "not enough memory" case
        {
            CleanMemory();
            throw;
        }
    }

    Matrix2D(const Matrix2D& m)
        : Matrix2D(m._n)
    {
        for (int i = 0; i < _n; ++i)
            memcpy(_matrix[i], m._matrix[i], _n * sizeof(Element));
    }

    ~Matrix2D()
    {
        CleanMemory();
    }

    int Size() const { return _n; }

    const Element* operator[](unsigned int x) const
    {
        return _matrix[x];
    }
    Element*& operator[](unsigned int x)
    {
        return _matrix[x];
    }
};

