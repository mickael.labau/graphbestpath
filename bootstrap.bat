@echo off

setlocal 
set nopause=0

:parsing_arg_loop
      :: Thanks to https://stackoverflow.com/a/34552964
      ::-------------------------- has argument ?
	  :: in %~1 - the ~ removes any wrapping " or '.
      if ["%~1"]==[""] (
        goto parsing_arg_end
      )
      ::-------------------------- argument exist ?
	  if ["%~1"]==["--nopause"] set nopause=1

      ::--------------------------
      shift
      goto parsing_arg_loop


:parsing_arg_end


echo.
echo  ** Loading git submodule (if any)
REM remove empty folder "ApplicationBase" so commandline "git submodule update ..." works 
REM (Why git create a empty folder on cloning ?! I thought git only handles files not folders...
REM  So how it is possible ?)
ROBOCOPY libs\rapidjson libs\rapidjson /S /MOVE >NUL
echo git submodule update --init --recursive
git submodule update --init --recursive

cd libs\rapidjson
REM Nothing to do to init submodule here
cd ..\..

echo ** Make all git repositories "standalone" **
"Git Submodule undo absorbgitdirs (Gui).exe" --noGui

if ["%nopause%"] == ["0"] (
  echo DONE! Press any key to leave this script
  pause
)
endlocal
